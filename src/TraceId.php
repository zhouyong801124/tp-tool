<?php
// +----------------------------------------------------------------------
// | Author: 冰蓝工作室
// +----------------------------------------------------------------------
// | Email: zhouyong_0@hotmail.com
// +----------------------------------------------------------------------
// | Date: 2021/11/17 22:00
// +----------------------------------------------------------------------
// | DESC: 文件名称TraceId.php
// +----------------------------------------------------------------------
// | Copyright (c) 2021-2025 rights reserved.
// +----------------------------------------------------------------------
namespace LogTrace;


/**
 * 利用雪花算法生成唯一的TraceId
 * Class TraceId
 * @package LogTrace
 */
class TraceId
{
    /**
     * TraceId
     * @var null | string
     */
    protected static $traceId = null;

    /**
     * 重置TraceId
     */
    public static function reset()
    {
        self::$traceId = null;
    }

    /**
     * @param string|null $traceId
     */
    public static function setTraceId($traceId)
    {
        self::$traceId = $traceId;
    }

    /**
     * @return string|null
     */
    public static function getTraceId()
    {
        if (self::$traceId == null) {
            $id =  IdCreate::createOnlyId();
            self::$traceId = md5($id);
        }
        return self::$traceId;
    }


}