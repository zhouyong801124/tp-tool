<?php
// +----------------------------------------------------------------------
// | Author: 冰蓝工作室
// +----------------------------------------------------------------------
// | Email: zhouyong_0@hotmail.com
// +----------------------------------------------------------------------
// | Date: 2021/11/17 22:00
// +----------------------------------------------------------------------
// | DESC: 文件名称IdCreate.php
// +----------------------------------------------------------------------
// | Copyright (c) 2021-2025 rights reserved.
// +----------------------------------------------------------------------
namespace LogTrace;

/**
 * 使用雪花算法生成唯一ID
 * Class IdCreate
 * @package LogTrace
 */
class IdCreate
{
    private static $workerId = 1;               // 工作机器ID
    private static $datacenterId = 1;           // 数据中心ID
    private static $sequence = 0;

    /**
     * @var null | SnowFlake
     */
    private static $snowFlake = null;

    /**
     * @param int $workerId
     */
    public static function setWorkerId($workerId)
    {
        self::$workerId = $workerId;
    }

    /**
     * @param int $datacenterId
     */
    public static function setDatacenterId($datacenterId)
    {
        self::$datacenterId = $datacenterId;
    }

    /**
     * @param int $sequence
     */
    public static function setSequence($sequence)
    {
        self::$sequence = $sequence;
    }

    public static function createOnlyId()
    {
        if (self::$snowFlake == null) {
            self::$snowFlake = new SnowFlake(self::$workerId, self::$datacenterId, self::$sequence);
        }
        return self::$snowFlake->nextId();
    }

}