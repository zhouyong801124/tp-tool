<?php
// +----------------------------------------------------------------------
// | Author: 冰蓝工作室
// +----------------------------------------------------------------------
// | Email: zhouyong_0@hotmail.com
// +----------------------------------------------------------------------
// | Date: 2021/11/17 22:00
// +----------------------------------------------------------------------
// | DESC: 文件名称SnowFlake.php
// +----------------------------------------------------------------------
// | Copyright (c) 2021-2025 rights reserved.
// +----------------------------------------------------------------------
namespace LogTrace;
use Exception;

/**
 * 雪花算法
 * Class SnowFlake
 * @package LogTrace
 */
class SnowFlake
{
    const TWEPOCH = 1637333552530;                      // 时间起始标记点，作为基准，一般取系统的最近时间（一旦确定不可变动）

    const WORKER_ID_BITS        = 5;                    // 机器标识位数
    const DATACENTER_ID_BITS    = 5;                    // 数据中心标识位数
    const SEQUENCE_BITS         = 12;                   // 毫秒内自增位

    private $workerId;                                  // 工作机器ID
    private $datacenterId;                              // 数据中心ID
    private $sequence;                                  // 毫秒内序列

    private $maxWorkerId        = -1 ^ (-1 << self::WORKER_ID_BITS);    // 机器ID最大值
    private $maxDatacenterId    = -1 ^ (-1 << self::DATACENTER_ID_BITS);//数据中心ID最大值

    private $workerIdShift      = self::SEQUENCE_BITS;  // 机器ID偏左移位数
    private $datacenterIdShift  = self::SEQUENCE_BITS + self::WORKER_ID_BITS;   // 数据中心左移位数
    private $timestampLeftShift = self::SEQUENCE_BITS + self::WORKER_ID_BITS + self::DATACENTER_ID_BITS;    // 时间毫秒左移位数
    private $sequenceMask       = -1 ^ (-1 << self::SEQUENCE_BITS);     // 生成序列的掩码

    private $lastTimestamp = -1;                        // 上次生产id时间戳

    /**
     * 构造函数
     * @param $workerId
     * @param $datacenterId
     * @param int $sequence
     * @throws Exception
     */
    public function __construct($workerId, $datacenterId, $sequence = 0)
    {
        // 机器ID不能大于最大值或小于0
        if ($workerId > $this->maxWorkerId || $workerId < 0) {
            throw new Exception("worker Id can't be greater than {$this->maxWorkerId} or less than 0");
        }
        // 数据中心ID不能大于最大值或小于0
        if($datacenterId > $this->maxDatacenterId || $datacenterId < 0) {
            throw new Exception("datacenter Id can't be greater than {$this->maxDatacenterId} or less than 0");
        }
        // 初始化变量
        $this->workerId = $workerId;
        $this->datacenterId = $datacenterId;
        $this->sequence = $sequence;
    }

    /**
     * 生成ID
     * @return int
     * @throws Exception
     */
    public function nextId()
    {
        $timestamp = $this->timeGen();

        if ($timestamp < $this->lastTimestamp) {
            // 两个任意精度数字的减法
            $diffTimestamp = bcsub($this->lastTimestamp, $timestamp);
            throw new Exception("Clock moved backwards. Refusing to generate id for {$diffTimestamp} milliseconds");
        }

        if ($this->lastTimestamp == $timestamp)
        {
            $this->sequence = ($this->sequence + 1) & $this->sequenceMask;

            if (0 == $this->sequence) {
                $timestamp = $this->tilNextMillis($this->lastTimestamp);
            }
        } else {
            $this->sequence = 0;
        }

        $this->lastTimestamp = $timestamp;

        return (($timestamp - self::TWEPOCH) << $this->timestampLeftShift) |
            ($this->datacenterId << $this->datacenterIdShift) |
            ($this->workerId << $this->workerIdShift) |
            $this->sequence;
    }

    protected function tilNextMillis($lastTimestamp)
    {
        $timestamp = $this->timeGen();
        while ($timestamp <= $lastTimestamp) {
            $timestamp = $this->timeGen();
        }

        return $timestamp;
    }

    /**
     * 返回当前系统时间戳（毫秒）
     * @return false|float
     */
    protected function timeGen()
    {
        return floor(microtime(true) * 1000);
    }
}